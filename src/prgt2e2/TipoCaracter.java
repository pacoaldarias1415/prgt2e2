/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt2e2;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class TipoCaracter
{
   public static void main(String[] arg)
   {
      char valor, valor1;
      // el caracter tiene que estar encerrado entre comillas simples.
      valor = 'A'; 
      valor1 = 65;
      System.out.println("Caracter "+valor);
      System.out.println("Caracter "+valor1);
   }
} 
/*
EJECUCION:
Caracter A
Caracter A
*/