/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt2e2;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Asignacion {

    public static void main(String args[]) {
        int a = 1;

        a += 2;
        System.out.println("1. " + a);
        a /= 2;
        System.out.println("2. " + a);
    }
}
/* EJECUCION:
1. 3
2. 1
*/